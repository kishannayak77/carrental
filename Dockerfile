FROM node:10-alpine as builder
WORKDIR /app
COPY . .
RUN npm install
RUN npm install bootstrap@4 jquery --save
RUN $(npm bin)/ng build --prod

# stage 2
FROM nginx:1.14.1-alpine
COPY --from=builder /app/dist/CarRental /usr/share/nginx/html
