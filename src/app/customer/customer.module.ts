import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerRoutingModule } from './customer-routing.module';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ContactComponent } from './contact/contact.component';
import { RegisterComponent } from './register/register.component';
import { ServiceComponent } from './service/service.component';
import { AboutComponent } from './about/about.component';

@NgModule({
  declarations: [HomeComponent, LoginComponent, ContactComponent, RegisterComponent, ServiceComponent, AboutComponent],
  imports: [
    CommonModule,
    CustomerRoutingModule
  ]
})
export class CustomerModule { }
